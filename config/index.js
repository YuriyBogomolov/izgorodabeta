var cwd = process.cwd();
var fs = require('fs');
var env = process.env.NODE_ENV || 'development';
var path = require('path');

var result = { env: env };

fs.readdirSync(path.join(cwd, 'config', env)).forEach(function (file) {
    if (file[0] !== '.') {
        result[file.slice(0, file.indexOf('.'))] = require(path.join(cwd, 'config', env, file));
    }
});

module.exports = result;