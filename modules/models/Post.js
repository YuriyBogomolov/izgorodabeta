var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var Comment = mongoose.model('Comment');
var sanitizer = require('sanitizer');

var Promise = require('bluebird');

var PostSchema = new Schema({
  permalink: { type: String },
  title: { type: String, required: true },
  author: { type: ObjectId, required: true, ref: 'User' },
  body: { type: String, required: true },
  date_published: { type: Date, required: true },
  comments: [{ type: ObjectId, ref: 'Comment' }],
  tags: [String],
  is_special: { type: Boolean, default: false },
  image_url: String
});

PostSchema.statics = {
  list: function (options) {
    var criteria = options.criteria || {};

    return this.find(criteria)
        .populate('author', 'username avatar_url')
        .sort({'date_published': -1}) // sort by date
        .limit(options.perPage)
        .skip(options.perPage * options.page)
        .exec();
  },

  load: function(permalink) {
    return this.findOne({ permalink: permalink })
        .populate('author', 'username avatar_url')
        .populate('comments', 'author body date')
        .populate('comments.author', 'username avatar_url')
        .exec();
  }
};

PostSchema.methods = {
  addComment: function (user, comment) {
    var newComment = new Comment();
    newComment.author = user._id;
    newComment.body = sanitizer.escape(comment.body);
    newComment.body = newComment.body.replace(/\r\n/g, "<br/>");
    newComment.date = new Date();
    newComment.permalink = this.permalink;

    var that = this;
    return newComment.save().then(function(savedComment) {
        that.comments.push(savedComment._id);
        return that.save();
    });
  }
};

module.exports = mongoose.model('Post', PostSchema);