var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var DealSchema = new Schema({
  title: String,
  body: String,
  date_published: { type: Date, default: (new Date()).getTime() }
});

DealSchema.statics = {
  list: function (options, callback) {
    var criteria = options.criteria || {};

    this.find(criteria)
        .sort({'date_published': -1}) // sort by date
        .limit(options.perPage)
        .skip(options.perPage * options.page)
        .exec(callback);
  },

  load: function(date_published, callback) {
    this.findOne({ date_published: date_published })
        .exec(callback);
  }
};

module.exports = mongoose.model('Deal', DealSchema);