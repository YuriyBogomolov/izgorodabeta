var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt   = require('bcrypt');
var oAuthTypes = ['facebook', 'google', 'vkontakte', 'twitter'];

var UserSchema = new Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  username: { type: String, default: '' },
  provider: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  authToken: { type: String, default: '' },
  last_login: { type: Date, default: new Date() },
  is_online: { type: Boolean, default: false },
  avatar_url: { type: String, default: '' },
  security_group: { type: String, default: 'Commentator' },
  facebook: {},
  google: {},
  vkontakte: {},
  twitter: {}
});

UserSchema
    .virtual('password')
    .set(function(password) {
      this._password = password;
      this.salt = bcrypt.genSaltSync(8);
      this.hashed_password = this.encryptPassword(password);
    })
    .get(function() { return this._password });

UserSchema.methods = {
  encryptPassword: function (password) {
    return bcrypt.hashSync(password, this.salt, null);
  },

  doesNotRequireValidation: function() {
    return ~oAuthTypes.indexOf(this.provider);
  },

  isValidPassword: function(password) {
    return this.encryptPassword(password) === this.hashed_password;
  },

  isBanned: function() {
    return this.security_group === 'Banned';
  }
};

module.exports = mongoose.model('User', UserSchema);