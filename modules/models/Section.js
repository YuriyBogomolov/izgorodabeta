var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var SectionSchema = new Schema({
  _id: Number,
  title: String,
  subsections: { type: Number, ref: 'Section' },
  posts: { type: ObjectId, ref: 'Post' }
});

module.exports = mongoose.model('Section', SectionSchema);