var Promise = require('bluebird');

module.exports = function (app, middleware) {
    app.get('/', middleware, function (req, res) {
        var Post    = req.models.Post;
        var Comment = req.models.Comment;

        var criteria = {
            tags: 'новость'
        };

        /*if (!security.hasSpecialAccess(req.user)) {
         criteria.is_special = false;
         }*/

        var page = (req.params.page || 1) - 1;
        var perPage = 30;
        var options = {
            perPage: perPage,
            page: page,
            criteria: criteria
        };

        return Promise.resolve().bind({}).then(function () {
            return Post.list(options);
        }).then(function(posts) {
            this.posts = posts;
            return Post.count(criteria).exec();
        }).then(function (count) {
            this.count = count;
            return Post.distinct('tags').exec();
        }).then(function (distinctTags){
            this.distinctTags = distinctTags;
            return Post.find(criteria).sort('-date_published').limit(5).exec();
        }).then(function (carouselPosts) {
            this.carouselPosts = carouselPosts;
            return Comment
                .find({}, {"author": 1, "body": 1, "date": 1, "permalink": 1})
                .sort({"date":-1})
                .limit(5)
                .populate('author', 'username')
                .exec();
        }).then(function (recentComments){
            res.json({
                title: 'Блог Клуба «КудЫ» | Записи',
                posts: this.posts,
                page: page + 1,
                pages: Math.ceil(this.count / perPage),
                recentPosts: this.carouselPosts,
                user: req.user,
                tagcloud: this.distinctTags,
                recentComments: recentComments
            });
        }).then(null, function (err) {
            return res.status(500).json({ error: err.message });
        });
    });
};