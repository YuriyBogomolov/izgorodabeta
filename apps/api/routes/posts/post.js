var Promise = require('bluebird');
var deepExtend = require('deep-extend');

module.exports = function (app, middleware) {
    app.post('/:permalink', middleware, function (req, res) {
        var Post = req.models.Post;

        Promise.resolve().bind({}).then(function () {
            return Post.load(req.params.permalink);
        }).then(function (post) {
            deepExtend(post, req.body);

            return new Promise(function (resolve, reject) {
                post.save(function (err, data) {
                    if (err) {
                        reject(err);
                        return;
                    }

                    resolve(data);
                });
            });
        }).then(function (post) {
            res.json(post);
        }).catch(function (err) {
            res.status(500).json({ error: err.message });
        });
    });
};