var Promise = require('bluebird');

module.exports = function (app, middleware) {
    app.get('/:permalink', middleware, function (req, res) {
        var Post = req.models.Post;

        Promise.resolve().bind({}).then(function () {
            return Post.load(req.params.permalink);
        }).then(function (post) {
            res.json(post);
        }).catch(function (err) {
            res.status(500).json({ error: err.message });
        });
    });
};