var cwd = process.cwd();
var util = require('util');
var require_directory = require('require-directory');
var fs = require('fs');
var path = require('path');

function requireDirectory(path) {
    if (fs.existsSync(path)) {
        return require_directory(module, path);
    }
    return [];
}

function injectRoute(baseDir, baseNS, common_middleware) {
    var routes = requireDirectory(baseDir);
    var paths = [];

    Object.keys(routes).forEach(function (route) {
        var middleware = requireDirectory(baseDir.replace('/routes/', '/routes/middleware/') + route);
        var methods = routes[route];

        var route_middleware = [];
        Object.keys(common_middleware).forEach(function (middleware_name) {
            route_middleware.push(common_middleware[middleware_name]);
        });

        Object.keys(middleware).forEach(function (middleware_name) {
            route_middleware.push(middleware[middleware_name]);
        });

        if (typeof methods == 'function') {
            paths.push({
                route: baseNS,
                method: methods,
                middleware: route_middleware
            });
        } else {
            Object.keys(methods).forEach(function (method_name) {
                if (typeof methods[method_name] == 'function') {
                    paths.push({
                        route: baseNS + route,
                        method: methods[method_name],
                        middleware: route_middleware
                    });
                } else {
                    paths = paths.concat(injectRoute(path.join(baseDir, route, method_name), path.join(baseNS, route, method_name.replace('_', ':')), common_middleware));
                }
            });
        }
    });

    return paths;
}

module.exports = function(app) {
    var common_middleware = requireDirectory(cwd + '/apps/api/routes/middleware/common');
    var paths = injectRoute(cwd + '/apps/api/routes', '/api/', common_middleware);

    paths.forEach(function (route) {
        app.namespace(route.route, function () {
            route.method(app, route.middleware);
        });
    });
};