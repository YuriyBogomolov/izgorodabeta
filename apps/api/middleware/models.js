var cwd = process.cwd();
var Promise = require('bluebird');

module.exports = function (req, res, next) {
    req.models = require(cwd + '/modules/models');
    next();
};