var cwd = process.cwd();
var config = require(cwd + '/config');

var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var routes = require('./routes');
var mongoose = require('mongoose');
var models = require('./middleware/models');

require('express-namespace');

var app = express();
var server = http.createServer(app);
app.set('server', server);

mongoose.connect(config.db.address);
config.session.store = new MongoStore({ mongooseConnection: mongoose.connection });

app.use(bodyParser.json({ limit: '50mb' }));
app.use(session(config.session));
app.use(models);

routes(app);

server.listen(config.server.api.port, function () {
    console.log("IzGoroda api app (http://localhost:" + config.server.api.port + ") started on " + (new Date()));
    console.log('Environment: ' + config.env);
});