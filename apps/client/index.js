var cwd = process.cwd();
var config  = require(cwd + "/config");
var http = require('http');
var express = require('express');
var Mincer = require('mincer');
var ejs = require('ejs');
var fs = require('fs');
var html2js = require('ng-html2js');
var Promise = require('bluebird');
var dir = require('node-dir');
var favicon = require('serve-favicon');
var path  = require('path');

var compileTemplates = function (baseDir) {
    var templates = '(function PHAngularTemplates() { ';

    return new Promise(function (resolve, reject) {
        dir.readFiles(baseDir,
            {
                recursive: true,
                match: /.*html$/,
                exclude: /index.html$/
            },
            function(err, content, filename, next) {
                if(err) {
                    return reject(err);
                }

                templates += html2js(filename.replace(baseDir, ''), content, 'client', 'module');
                next();
            },
            function(err) {
                if(err) {
                    return reject(err);
                }

                templates += '})()';
                return resolve(templates);
            }
        );
    });
};

var indexHtmlFile = cwd + '/apps/client/public/index.html';
var cliVersion = fs.existsSync(cwd + '/version') ? fs.readFileSync(cwd + '/version') : Date.now();

fs.writeFileSync(indexHtmlFile, ejs.render(fs.readFileSync(indexHtmlFile + '.ejs').toString(), { version: cliVersion }));

if (config.env !== 'development') {
    compileTemplates(cwd + '/apps/client/public').then(
        function (templates) {
            fs.writeFileSync(cwd + '/apps/client/assets/scripts/angular-templates.js', templates);
        },
        function (err) {
            throw err;
        }
    );
}

var app = express();
var server = http.createServer(app);
app.set('server', server);
app.use(favicon(path.join(__dirname, 'assets', 'images','favicon.ico')));

if (config.mincer.log) {
    Mincer.logger.use(console);
}

var mincerEnv = new Mincer.Environment();

mincerEnv.registerHelper('host', function () {
    return config.client.host;
});

mincerEnv.appendPath(cwd + '/vendor');
mincerEnv.appendPath(cwd + '/apps/client/assets/images');
mincerEnv.appendPath(cwd + '/apps/client/assets/scripts');
mincerEnv.appendPath(cwd + '/apps/client/assets/stylesheets');
mincerEnv.appendPath(cwd + '/apps/client/assets/');

app.use('/', express.static(cwd + '/apps/client/public'));
app.use('/assets', Mincer.createServer(mincerEnv));
app.use('/ckeditor', express.static(cwd + '/modules/ckeditor'));

server.listen(config.server.port, function () {
    console.log("IzGoroda client app (http://localhost:" + config.server.port + ") started on " + (new Date()));
    console.log('Environment: ' + config.env);
});