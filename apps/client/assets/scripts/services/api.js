function API ($http) {
    this.$http = $http;

    this.home = new APIHome(this);
    this.posts = new APIPost(this);
}

ClientApp.factory('api', ['$http', function ($http) {
    return new API($http);
}]);