function Locations ($location) {
    this.$location = $location;
}

Locations.prototype.home = function () {
    this.$location.path('/');
};

Locations.prototype.isHome = function () {
    return this.$location.path() === '/';
};

Locations.prototype.post = function (permalink) {
    this.$location.path('/posts/' + permalink);
};

ClientApp.factory('locations', ['$location', function ($location) {
    return new Locations($location);
}]);