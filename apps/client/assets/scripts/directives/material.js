ClientApp.directive('materialCheckbox', function () {
    return {
        restrict: 'E',
        template: '<div class="checkbox"><label><input type="checkbox" ng-model="model"><span class="ripple"></span><span class="check"></span>{{header}}</label></div>',
        require: 'ngModel',
        scope: {
            header: '@',
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelController) {
            ngModelController.$viewChangeListeners.push(function() {
                $scope.$eval(attrs.ngChange);
            });

            $scope.toggle = function () {
                ngModelController.$setViewValue($scope.model);
            };

            element.on("change", ".checkbox input[type=checkbox]", function() { $(this).blur(); });
        }
    };
});

ClientApp.directive('materialRadio', function () {
    return {
        restrict: 'E',
        template: '<div class="radio {{class}}"><label><input type="radio" name="{{name}}" ng-checked="check">{{header}}<span class="circle"></span><span class="check"></span></label></div>',
        scope: {
            class: '=',
            header: '@',
            name: '@',
            check: '=ngChecked'
        },
        link: function ($scope, element, attrs) {}
    };
});

ClientApp.directive('materialInput', function () {
    return {
        restrict: 'E',
        template: '<div class="form-control-wrapper material-padded"><input type="{{type}}" class="form-control" ng-model="model" id="{{inputId}}" name="{{name}}" ng-change="changed()" step="{{step}}" min="{{min}}"><div class="hint" ng-show="hint">{{hint}}</div><div class="floating-label">{{placeholder}}</div><span class="material-input"></span></div>',
        require: 'ngModel',
        scope: {
            placeholder: '@',
            type: '@',
            hint: '@',
            model: '=ngModel',
            inputId: '@',
            name: '@',
            step: '@',
            min: '@',
            disabled: '@'
        },
        link: function ($scope, element, attrs, ngModelController) {
            var input = element.find('input');

            ngModelController.$viewChangeListeners.push(function() {
                $scope.$eval(attrs.ngChange);
            });

            $scope.changed = function () {
                ngModelController.$setViewValue($scope.model);
            };

            if ($scope.disabled) {
                input.attr('disabled', $scope.disabled);
            }

            $scope.$watch(function () {
                if (!input.hasClass('ng-invalid') && ($scope.model === undefined || $scope.model === null || $scope.model === '')) {
                    input.addClass("empty");
                } else {
                    input.removeClass("empty");
                }
            });
        }
    };
});

ClientApp.directive('materialTextarea', function () {
    return {
        restrict: 'E',
        template: '<div class="form-control-wrapper material-padded"><textarea class="form-control" ng-model="model" id="{{inputId}}" name="{{name}}" ng-change="changed()" rows="{{rows}}" cols="{{cols}}"></textarea><div class="hint" ng-show="hint">{{hint}}</div><div class="floating-label">{{placeholder}}</div><span class="material-input"></span></div>',
        require: 'ngModel',
        scope: {
            placeholder: '@',
            hint: '@',
            model: '=ngModel',
            inputId: '@',
            name: '@',
            rows: '@',
            cols: '@'
        },
        link: function ($scope, element, attrs, ngModelController) {
            var textarea = element.find('textarea');

            ngModelController.$viewChangeListeners.push(function() {
                $scope.$eval(attrs.ngChange);
            });

            $scope.changed = function () {
                ngModelController.$setViewValue($scope.model);
            };

            $scope.$watch(function () {
                if (!textarea.hasClass('ng-invalid') && ($scope.model === undefined || $scope.model === null || $scope.model === '')) {
                    textarea.addClass("empty");
                } else {
                    textarea.removeClass("empty");
                }
            });
        }
    };
});

ClientApp.directive('materialToggle', function () {
    return {
        restrict: 'E',
        template: '<div class="togglebutton"><label title="{{header}}"><input type="checkbox" ng-model="model" ng-change="toggle()"><span class="toggle"></span></label></div>',
        require: 'ngModel',
        scope: {
            header: '@',
            model: '=ngModel'
        },
        link: function ($scope, element, attrs, ngModelController) {
            ngModelController.$viewChangeListeners.push(function() {
                $scope.$eval(attrs.ngChange);
            });

            $scope.toggle = function () {
                ngModelController.$setViewValue($scope.model);
            };
        }
    };
});

ClientApp.directive('materialFab', function () {
    return {
        restrict: 'E',
        template: '<div class="fab-material-wrapper"><a class="btn btn-fab {{class}} {{icon}}" title="{{title}}" ng-click="action()"></a></div>',
        scope: {
            icon: '@',
            title: '@',
            class: '@',
            action: '='
        },
        link: function ($scope, element, attrs) {}
    };
});