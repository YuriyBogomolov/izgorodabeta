var ClientApp = angular.module('client', [
    'ngRoute',
    'ngTouch',
    'ngAnimate',
    'ngMaterial'
]);

var CKEDITOR_BASEPATH = 'ckeditor/';

ClientApp.routing = [];

ClientApp.config(['$routeProvider', '$animateProvider', function($routeProvider, $animateProvider) {
    $animateProvider.classNameFilter(/angular-animate/);
    ClientApp.routing.forEach(function(route) {
        route($routeProvider);
    });
}]);

ClientApp.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        //.primaryPalette('blue-grey')
        .primaryPalette('green')
        //.accentPalette('amber');
        .accentPalette('orange');
});

ClientApp.run(function () {
});