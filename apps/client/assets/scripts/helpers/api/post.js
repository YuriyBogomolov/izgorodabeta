function APIPost (api) {
    this.api = api;
}

APIPost.prototype.get = function (permalink) {
    return this.api.$http.get('/api/posts/' + permalink).then(function (result) {
        return result.data;
    });
};

APIPost.prototype.save = function (post) {
    return this.api.$http.post('/api/posts/' + post.permalink, post).then(function (result) {
        return result.data;
    });
};