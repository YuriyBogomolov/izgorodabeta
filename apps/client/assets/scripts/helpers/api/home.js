function APIHome (api) {
    this.api = api;
}

APIHome.prototype.get = function () {
    return this.api.$http.get('/api/home').then(function (result) {
        return result.data;
    });
};