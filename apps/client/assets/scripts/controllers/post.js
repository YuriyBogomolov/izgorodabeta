function PostController ($scope, $routeParams, api, locations) {
    $scope.locations = locations;

    api.posts.get($routeParams.permalink).then(function (data) {
        $scope.post = data;
    });
}

ClientApp.controller('PostController', ['$scope', '$routeParams', 'api', 'locations', PostController]);