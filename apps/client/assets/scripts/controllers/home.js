function HomeController ($scope, api, locations, $timeout, $mdSidenav) {
    $scope.locations = locations;

    api.home.get().then(function (data) {
        $scope.posts = data.posts;
        $scope.page = data.page;
        $scope.pages = data.pages;
        $scope.recentPosts = data.recentPosts;
        $scope.tagcloud = data.tagcloud;
        $scope.recent_comments = data.recentComments;

        //$('#recentPosts').carousel();
        console.log(data);
    });

    $scope.read = function () {
        locations.post(this.post.permalink);
    };
}

ClientApp.controller('HomeController', ['$scope', 'api', 'locations', '$timeout', '$mdSidenav', HomeController]);