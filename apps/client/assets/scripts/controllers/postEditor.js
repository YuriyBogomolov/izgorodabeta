function PostEditorController ($scope, $routeParams, api, locations, $mdToast) {
    $scope.locations = locations;

    $scope.options = {
        language: 'ru',
        allowedContent: true,
        entities: false
    };

    api.posts.get($routeParams.permalink).then(function (data) {
        console.log(data);
        $scope.post = data;
    });

    $scope.save = function () {
        api.posts.save($scope.post).then(function (data) {
            $mdToast.showSimple('Измненения сохранены');
        });
    };
}

ClientApp.controller('PostEditorController', ['$scope', '$routeParams', 'api', 'locations', '$mdToast', PostEditorController]);