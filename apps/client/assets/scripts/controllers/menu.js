function MenuController ($rootScope, $scope, locations, api, $mdDialog, $mdSidenav) {
    var self = this;
    self.$scope = $scope;
    self.user = null;

    $scope.signIn = function () {
        locations.signIn();
    };

    $scope.signOut = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Действительно выйти?')
            .content('Пожалуйста, подтвердите, что Вы хотите выйти из системы.')
            .ariaLabel('Подтверждение выхода')
            .ok('Выйти')
            .cancel('Остаться')
            .targetEvent(ev);
        $mdDialog.show(confirm).then(function() {
            locations.signOut();
        });
    };

    $scope.home = function () {
        locations.home();
    };

    $scope.profile = function () {
        locations.profile();
    };

    $scope.usersList = function () {
        locations.users();
    };

    $scope.showSidebar = function () {
        $mdSidenav('left').open();
    };

    self.init();
}

MenuController.prototype.init = function () {
    var $scope = this.$scope;

    $scope.menuItems = [
        {
            buttonClass: 'md-default',
            iconClass: 'mdi-information-outline',
            title: 'О Клубе',
            action: $scope.about,
            disabled: false
        },
        {
            isDivider: true
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Анонсы',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Обзоры',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Статьи',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Тренинги',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Техника',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Страйкбол',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: '',
            title: 'Экспедиции',
            action: $scope.about,
            disabled: false
        },
        {
            isDivider: true
        },
        {
            buttonClass: 'md-default',
            iconClass: 'mdi-file-image-box',
            title: 'Фотогалерея',
            action: $scope.about,
            disabled: false
        },
        {
            buttonClass: 'md-default',
            iconClass: 'mdi-video',
            title: 'Видео',
            action: $scope.about,
            disabled: false
        },
        {
            isDivider: true
        }
    ];

    if (this.user) {
        $scope.menuItems = $scope.menuItems.concat([
            {
                isDivider: true
            },
            {
                buttonClass: 'md-default',
                iconClass: 'mdi-account-multiple',
                title: 'Управление пользователями',
                action: $scope.usersList
            }
        ]);
    } else {
        $scope.menuItems = $scope.menuItems.concat([
            {
                isDivider: true
            },
            {
                buttonClass: 'md-primary',
                iconClass: 'mdi-login',
                title: 'Войти',
                action: $scope.signIn
            }
        ]);
    }
};

ClientApp.controller('MenuController', ['$rootScope', '$scope', 'locations', 'api', '$mdDialog', '$mdSidenav', MenuController]);
