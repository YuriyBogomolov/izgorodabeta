ClientApp.routing.push(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl    : '/views/home.html',
        controller     : 'HomeController',
        reloadOnSearch : false,
        resolve        : {
            //person: resolveUser
        }
    });
});