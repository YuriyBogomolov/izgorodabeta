ClientApp.routing.push(function($routeProvider) {
    $routeProvider.when('/posts/:permalink', {
        templateUrl    : '/views/posts/post.html',
        controller     : 'PostController',
        reloadOnSearch : false,
        resolve        : {
            //person: resolveUser
        }
    });

    $routeProvider.when('/posts/:permalink/edit', {
        templateUrl    : '/views/posts/edit.html',
        controller     : 'PostEditorController',
        reloadOnSearch : false,
        resolve        : {
            //person: resolveUser
        }
    });
});