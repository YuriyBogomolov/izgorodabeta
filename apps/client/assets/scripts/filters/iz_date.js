ClientApp.filter('iz_date', function($filter) {
    var angularDateFilter = $filter('date');
    return function(date) {
        return angularDateFilter(date, 'dd.MM.yyyy');
    };
});

ClientApp.filter('rawHtml', ['$sce', function($sce){
    return function(val) {
        return $sce.trustAsHtml(val);
    };
}]);

ClientApp.filter('cut', [function () {
    return function (body) {
        return body.replace(/<cut.*<\/cut>([\r\n|\n|\r]*.*)*/gim, '');
    }
}]);